<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
        integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    
    <!-- style  css -->
    <link rel="stylesheet" type="text/css" href="../css/style.css">
    <link rel="stylesheet" type="text/css" href="../css/responsive.css">

    <title>SI - Penjadwalan Dosen</title>

</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-light">
        <div class="container">
        <a class="navbar-brand" href="#">UNDIKSHA</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup"
            aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav ml-auto">
            <a class="nav-link" href="http://localhost/crud/jadwal/">Jadwal</a>
            <a class="nav-link active" href="http://localhost/crud/dosen/">Dosen</a>
            <a class="nav-link" href="http://localhost/crud/kelas/">Kelas</a>
            </div>
        </div>
        </div>
    </nav>
    <div class="jumbotron jumbotron-fluid">
        <div class="container mt-5">
            <h1 class="display-4">Add Data</h1>
            <p class="lead">Isi Data Untuk Menambah Data Dosen</p>
        </div>
    </div>
    <div class="container">
        <form action="add.php" method="post" enctype="multipart/form-data">
            <div class="row">
                <div class="col-lg-6">
                    <div class="card col-12 mb-3" style="width: 60rem;">
                        <div class="card-body">
                            <div class="form-group">
                                <label class="control-label col-sm-4" for="nip_dosen">NIP DOSEN:</label>
                                <div class="col-sm-8">
                                    <input type="number" class="form-control" name="nip_dosen"
                                        placeholder="Masukkan NIP DOSEN" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="card col-12 mb-3" style="width: 60rem;">
                            <div class="card-body">
                                <label class="control-label col-sm-4" for="nama_dosen">Nama Lengkap:</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="nama_dosen"
                                        placeholder="Masukkan Nama" required>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="card col-12 mb-3" style="width: 60rem;">
                        <div class="card-body">
                            <div class="form-group">
                                <label class="control-label col-sm-4" for="prodi">Program Studi:</label>
                                <div class="col-sm-8">
                                    <input name="prodi" class="form-control" placeholder="Masukkan Program Studi"
                                        required></input>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="card col-12 mb-3" style="width: 60rem;">
                            <div class="card-body">
                                <label class="control-label col-sm-4" for="fakultas">Fakultas:</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="fakultas"
                                        placeholder="Masukkan Fakultas" required>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> <!-- col -->
            </div> <!-- row -->
            <div class="row">
                <div class=" col-lg-12 ">
                    <div class="card col-12 mb-3">
                        <div class="card-body">
                            <div class="form-group">
                                <label class="control-label col-sm-4" for="foto">Foto:</label>
                                <div class="col-sm-8">
                                    <input type="file" class="form-control" name="foto_dosen" required>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <button type="submit" name="submit" class="btn btn-primary" >Register Data</button>
            <a href='index.php' class="btn btn-danger">Batal</a>
        </form>
    </div>
    <footer class="mt-5">
        <div class="container">
            <p class="text-center">&copy Dyah Kusuma</p>
        </div>
    </footer>



    

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
        crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns"
        crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js" integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF" crossorigin="anonymous"></script>
    -->
</body>

</html>