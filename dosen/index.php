<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
    integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">

  <!-- style  css -->
  <link rel="stylesheet" type="text/css" href="../css/style.css">
  <link rel="stylesheet" type="text/css" href="../css/responsive.css">

  <title>SI - Penjadwalan Dosen</title>

</head>

<body>
  <!-- Image and text -->
  <nav class="navbar navbar-expand-lg navbar-light">
    <div class="container">
      <a class="navbar-brand" href="#">UNDIKSHA</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup"
        aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
        <div class="navbar-nav ml-auto">
          <a class="nav-link" href="http://localhost/crud/jadwal/">Jadwal</a>
          <a class="nav-link active" href="http://localhost/crud/dosen/">Dosen</a>
          <a class="nav-link" href="http://localhost/crud/kelas/">Kelas</a>
        </div>
      </div>
    </div>
  </nav>

  <div class="jumbotron jumbotron-fluid">
    <div class="container mt-5">
      <h1 class="display-4">Data Dosen</h1>
      <p class="lead">Create Read Update Delete!</p>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="table-responsive">
        <a href="tambah.php?id=<?php ?>" type="button" class="btn btn-success btn-sm"><span
            class="glyphicon glyphicon-trash"></span> Tambah DATA</a>
        <hr>

        <table class="table table-dark table-stripped table-hover datatabel ">
          <thead>
            <tr>
              <th>No</th>
              <th>Foto</th>
              <th>Nip</th>
              <th>Nama</th>
              <th>Prodi</th>
              <th>Fakultas</th>
              <th>Opsi</th>
            </tr>
          </thead>
          <tbody>
            <?php
            include 'koneksi.php';
            $i = 1;
            $dosen_view = mysqli_query($connect, "select * from dosen");
            while($d = mysqli_fetch_array($dosen_view))
            {
        ?>
            <tr>
              <td>
                <?php echo $i++; ?>
              </td>
              <td>
                <img src="gambar/<?php echo $d['foto_dosen'] ?>" width="100px" height="130px" />
              </td>
              <td>
                <?php echo $d['nip_dosen']; ?>
              </td>
              <td>
                <?php echo $d['nama_dosen']; ?>
              </td>
              <td>
                <?php echo $d['prodi']; ?>
              </td>
              <td>
                <?php echo $d['fakultas']; ?>
              </td>
              <td>
                <a href="update.php?id=<?php echo $d['id_dosen'];?>" type="button" class="btn
              btn-success btn-sm"><span class="glyphicon glyphicon-cog"></span> Edit</a>
                <a href="delete.php?id=<?php echo $d['id_dosen'];?>" type="button" class="btn btn-danger btn-sm"><span
                    class="glyphicon glyphicon-trash"></span> Hapus</a>
              </td>
             
            </tr>
            <?php
            }
          ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
  </div>
  <footer class="mt-5">
    <div class="container">
      <p class="text-center">&copy Dyah Kusuma</p>
    </div>
  </footer>
  <!--container-->

  <!-- Optional JavaScript; choose one of the two! -->

  <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
  <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
    integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
    crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns"
    crossorigin="anonymous"></script>

  <!-- Option 2: Separate Popper and Bootstrap JS -->
  <!--
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js" integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF" crossorigin="anonymous"></script>
    -->
</body>

</html>